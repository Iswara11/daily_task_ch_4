// setup awal

import http from "http";
import * as dotenv from "dotenv";
dotenv.config();
const PORT = process.env.PORT;
import { fileURLToPath } from "url";

// Ambil port dari environment variable

// Fungsi yang berjalan ketika ada request yang masuk.

import fs from "fs";
import path from "path";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const PUBLIC_DIRECTORY = path.join(__dirname, "public");

function renderCss(req, res) {
  if (req.url === "/style.css") {
    res.writeHead(200, { "Content-Type": "text/css" });
    const fileContent = fs.readFileSync("public/css/style.css", {
      encoding: "utf8",
    });
    res.write(fileContent);
    res.end();
  }
}

function getHTML(htmlFileName) {
  const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
  return fs.readFileSync(htmlFilePath, "utf-8");
}

const aset = path.join(__dirname, "public/css/");
const das = path.join(aset, "style.css");

// destructuring data
import { data1, data2, data3, data4, data5 } from "./shortData.js";

const Asset_DIRECTORY = path.join(__dirname, "asset");
const Json = fs.readFileSync(path.join(Asset_DIRECTORY, "data.json"));

// parse JSON
const fileJson = JSON.parse(Json);

// fungsi untuk menjalakan setiap url

function onRequest(req, res) {
  switch (req.url) {
    case "/":
      res.writeHead(200);
      res.end(getHTML("index.html"));
      return;
    case "/data":
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(Json);
      return;
    case "/data1":
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(data1(fileJson));
      return;
    case "/data2":
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(data2(fileJson));
      return;
    case "/data3":
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(data3(fileJson));
      return;
    case "/data4":
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(data4(fileJson));
      return;
    case "/data5":
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(data5(fileJson));
      return;
    case "/about":
      res.writeHead(200);
      res.end(getHTML("about.html"));
      return;
    case "/style.css":
      res.writeHead(200, { "Content-Type": "text/css" });
      var fileContents = fs.readFileSync(das, { encoding: "utf8" });
      res.write(fileContents);
      res.end();
      return;
    default:
      res.writeHead(404);
      res.end(getHTML("404.html"));
      return;
  }
}

// function toJSON(value) {
//   return JSON.stringify(value);
// }

const server = http.createServer(onRequest);

// Jalankan server
server.listen(PORT, "127.0.0.1", () => {
  console.log("Server sudah berjalan, silahkan buka http://127.0.0.1:%d", PORT);
});
